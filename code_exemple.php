<?
define("HIDE_SIDEBAR", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

// подключаем пространство имен класса HighloadBlockTable и даём ему псевдоним HLBT для удобной работы
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

//подключаем модуль highloadblock
CModule::IncludeModule('highloadblock');
//Напишем функцию получения экземпляра класса:
function GetEntityDataClass($HlBlockId) {
    if (empty($HlBlockId) || $HlBlockId < 1)
    {
        return false;
    }
    $hlblock = HLBT::getById($HlBlockId)->fetch();   
    $entity = HLBT::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    return $entity_data_class;
}

if($_POST["ac"] == "Y"){
	//use Bitrix\Main\Loader;

	CModule::IncludeModule("iblock");
	CModule::IncludeModuleEx("catalog");
	
	//Название мероприятия
   $HL_BLOCK_ID = 7;
	CModule::IncludeModule('highloadblock');
	$entity_data_class = GetEntityDataClass($HL_BLOCK_ID);
	$rsData = $entity_data_class::getList(array(
	   'select' => array('UF_XML_ID', 'UF_NAME')
	));
	while($el = $rsData->fetch()){
		if($el['UF_XML_ID'] == $_POST['SelectId_7']){ $wh_name = $el['UF_NAME'];}
	}
	
	$sq_p = 0;$q = 0;$t = 0;$d = 0;$k = 0;
	//Создаем массив ключей массива POST
	$key_post = array_keys ($_POST);
	//В массиве $key_post ищем ключи даты и времени
	foreach($key_post as $k1){
		if((strpos($k1, "p_date"))=== false){}
		else
		{$pd[$d] = $_POST[$k1];$d++;}
		if((strpos($k1, "p_time"))=== false){}
		else
		{$pt[$t] = $_POST[$k1];$t++;}
	}
	//Создаем двумерный! массив ряд-место-датавремя
	while ($q<$t){
		if (!$pd[$q] or !$pt[$q]){ $sq_datetime = false;$sq_array[$sq_p] = "No 'date' or 'time' specified";$sq_p++;}
			else
		{	
			//Значения датавремя
			$pdt[$q] = $DB->FormatDate($pd[$q]." ".$pt[$q], "YYYY-MM-DD HH:MI", "DD.MM.YYYY HH:MI");
			//В массиве $key_post ищем ключи ряд и место
			foreach($key_post as $f_rowseat){
				if((strpos($f_rowseat, "row"))=== false){}
					else
				{	//Разделяем строку ряд место на отдельные части массива
					$arr[$k] = explode("_", $f_rowseat);$arr[$k][2] = $pdt[$q];$k++;}
			}
		if($k == 0){ $sq_rowseat = false;$sq_array[$sq_p] = "No 'row' and 'seat' selected";$sq_p++;}
		}
	$q++;
	}
	//Не меняемые параметры
	$sq_iblock_id = "11";
	$sq_section = "26";
	$sq_total = "1";

	//Проверяем числовой пост параметр
	$sq_price = intval($_POST['p_price']);
	if (!$sq_price){ $sq_price = false;$sq_array[$sq_p] = "The transmission error of the numerical option prices";$sq_p++;}

	//Проверяем строковый пост параметр
	function sq_post_string($p_string) {
		$p_string = trim($p_string);
		$p_string = strip_tags($p_string);
		//$p_string = htmlspecialchars($p_string);
		$p_string = mysql_escape_string($p_string);
		global $sq_array;
		global $sq_p;
		if (!$p_string){ $p_string = false;$sq_array[$sq_p] = "String parameter transmission error";$sq_p++;}
		return $p_string;
    }
	$sq_det_tex = sq_post_string($_POST['p_det_tex']);
	$sq_hall = sq_post_string($_POST['SelectId_1']);
	$sq_seance =  sq_post_string($_POST['SelectId_5']);
	$sq_cinema = sq_post_string($_POST['SelectId_6']);
	$sq_show = sq_post_string($_POST['SelectId_7']);

	//Вывод ошибок
	if($sq_det_tex === false or $sq_price === false or $sq_hall === false or $sq_seance === false or $sq_cinema === false or $sq_show === false or $sq_datetime === false or $sq_rowseat === false){
		foreach($sq_array as $sq_arr){
			echo $sq_arr."<br>";
		}
		
	}
	//Скрипт прекратится если загруженный файл имеет формат php
	$blacklist = array(".php", ".phtml", ".php3", ".php4");
		foreach ($blacklist as $item) {
			if(preg_match("/$item\$/i", $_FILES['p_file']['name'])) {
			echo "We do not allow uploading PHP files!\n";
			exit;
		}
	}
	//Является ли файл изображением формата GIF, JPG, JPEG, GIF
	$imageinfo = getimagesize($_FILES['p_file']['tmp_name']);
	if($imageinfo['mime'] != 'image/gif' && $imageinfo['mime'] != 'image/jpeg' && $imageinfo['mime'] != 'image/jpg' && $imageinfo['mime'] != 'image/png') 
	{
		echo "Sorry, we only accept GIF, JPG, JPEG and GIF images\n";}
	else 
	{	// Каталог, в который мы будем принимать файл:
		$uploaddir = $_SERVER["DOCUMENT_ROOT"]."/img/";
		$uploadfile = $uploaddir.time().basename($_FILES['p_file']['name']);

		// Копируем файл из каталога для временного хранения файлов:
		if (copy($_FILES['p_file']['tmp_name'], $uploadfile))
		{ $sq_detailPicture = CFile::MakeFileArray($uploadfile);}
			else 
		{ echo "<h3>Error download file images!</h3>";}
	}
	
	
	foreach($arr as $value) 
	{ 
	$sq_row = $value['0'];
	$sq_seat = $value['1'];
	$sq_datetime = $value['2'];
	$sq_name = $wh_name." ".$p_date;
	$sq_code = $sq_show."_".$value['0']."_".$value['1']."_".$value['2']."_".time();

	//$sq_detailPicture = CFile::MakeFileArray($uploadfile); 
	
	//собираем массив полей
	$arFields = Array(
		"IBLOCK_ID"         => $sq_iblock_id,
		"IBLOCK_SECTION_ID" => $sq_section,
		"NAME"              => $sq_name,
		"CODE"              => $sq_code,
		"ACTIVE"            => "Y",
		"DETAIL_TEXT_TYPE"  => "html",
		"DETAIL_TEXT"       => $sq_det_tex,
		"DETAIL_PICTURE"    => $sq_detailPicture,
		"QUANTITY" 			=> "1",
		"CATALOG_GROUP_ID" => 1, //Базовая цена (у меня один тип цен)
		"PRICE" => "130",
		"CURRENCY" => "RUB",
		"PROPERTY_VALUES"   => Array(            // массив со свойствами инфоблока
			"HALL"  => $sq_hall,
			"SEAT" => $sq_seat,
			"ROW" => $sq_row,
			"SEANCE" => $sq_seance,
			"CINEMA" => $sq_cinema,
			"SHOW" => $sq_show,
			"DATE_TIME" => $sq_datetime,
		),
	);
	// создаем объект класса для работы
	$obElement = new CIBlockElement();

	// добавляем элемент, а ели не получается, то выводим ошибку
	$ID = $obElement->Add($arFields);
	if( $ID < 1 ) { echo $obElement->LAST_ERROR; }	else 
	{ 
		$productID = CCatalogProduct::add(array("ID" => $ID, "QUANTITY" => $sq_total,));
		if($productID == false){ echo "Failed to set the number";} else 
		{
			$arFields = Array(
			"CURRENCY"         => "RUB",       // валюта
			"PRICE"            => $sq_price,      // значение цены
			"CATALOG_GROUP_ID" => 1,           // ID типа цены
			"PRODUCT_ID"       => $ID,  // ID товара
			);
			// добавляем
			$CPiceAdd = CPrice::Add( $arFields );
			if($CPiceAdd == false){ echo "Failed to add price to item";}
		}
		echo "Ticket successfully added";
	}
	}
}
?>  
<form action="" method="post" name="form1" enctype="multipart/form-data">
	<input type="hidden" name="ac" value="Y">
	<? //Генерация списков параметров с помощью select
	$HL_B_ID = array('1','5','6','7');
	foreach($HL_B_ID as $Block_Id){?>
	   <?
		CModule::IncludeModule('highloadblock');
		$entity_data_class = GetEntityDataClass($Block_Id);
		$rsData = $entity_data_class::getList(array(
		   'select' => array('UF_XML_ID', 'UF_NAME')
		));?>
		<select name="SelectId_<?=$Block_Id?>">
		<?
		while($el = $rsData->fetch()){?>
			<option value="<?=$el['UF_XML_ID'];?>"><?=$el['UF_NAME'];?></option>
		<?}?>
   </select>
	<?}?>
	
	<?//Генерация ряда и мест с помощью checkbox
	$HL_BLOCK_ID = 4;
	CModule::IncludeModule('highloadblock');
	$entity_data_class = GetEntityDataClass($HL_BLOCK_ID);
	$rsData = $entity_data_class::getList(array(
	   'select' => array('UF_XML_ID', 'UF_NAME', 'UF_DESCRIPTION')
	));?>
	<?
	while($el = $rsData->fetch()){
		$row_sum = $el['UF_DESCRIPTION'];?>
		<p><?=$el['UF_NAME'];?></p>
		<ul name="ul_<?=$HL_BLOCK_ID?>" class="ul_<?=$HL_BLOCK_ID?>">
			<?//Генерация ряда и мест с помощью checkbox
			$HL_BLOCK_ID2 = 2;
			CModule::IncludeModule('highloadblock');
			$entity_data_class2 = GetEntityDataClass($HL_BLOCK_ID2);
			$rsData2 = $entity_data_class2::getList(array(
			   'select' => array('UF_XML_ID', 'UF_NAME')
			));?>
			<?
			//while($el2 = $rsData2->fetch()){
				$el2;
				for ($x=0, $el2 = $rsData2->fetch(); $x<$row_sum; $x++, $el2 = $rsData2->fetch()){
					$uf_xml =$el['UF_XML_ID']."_".$el2['UF_XML_ID'];
					$uf_xml_d =$el['UF_XML_ID']."_d_".$el2['UF_XML_ID'];?>
				
				<li>
					<label>
						<input type="checkbox" id="<?=$uf_xml?>" name="<?=$uf_xml?>"><?=$el2['UF_NAME'];?>
					</label>
				</li>
			<?}?>
		</ul></br>
	<?}?>
</br>
<input type="date" name="p_date0"><input type="time" name="p_time0">
<div id="input0"></div><div class="add" onclick="addInput()">+</div><br>
<input type="number" name="p_price" placeholder="Cost"/>
<input type="textarea" name="p_det_tex" placeholder="Text detail">
<input type="file" name="p_file" accept="image/png,image/jpeg,image/jpg,image/gif"><br>
<input name="submit" type="submit" value="GO PHP">
</form>
<script>
	var x = 0;

function addInput() {
	if (x < 10) {
    var str = '<input type="date" name="p_date' + (x + 1) + '"><input type="time" name="p_time' + (x + 1) + '"><div id="input' + (x + 1) + '"></div>';
    document.getElementById('input' + x).innerHTML = str;
    x++;
  } else
  {
  	alert('STOP it!');
  }
}
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>